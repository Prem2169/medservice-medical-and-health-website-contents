import $ from 'jquery';
import "../styles/style.css";
import MobileMenu from "./modules/MobileMenu";
import RevealOnScroll from "./modules/RevealOnScroll";

if(module.hot){
    module.hot.accept();
}
console.log("App.js is ready");

let mobileMenu = new MobileMenu();
new RevealOnScroll($(".section"), "40");
// alert("Every thing is fine");